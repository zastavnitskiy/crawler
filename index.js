#!/usr/bin/env node
const package = require('./package.json');
const program = require("commander");
const crawl = require("./lib/crawl");
const URL = require("url").URL;

function askForInput() {
  console.log(
    `Please specify a correct url with protocol, e.g. "-r http://google.com"`
  );
}

program
  .version("0.0.1")
  .description(package.description)
  .arguments("<url>")
  .option("-l, --limit <limit>", `how many pages you would like to crawl, defaults to ${crawl.DEFAULT_MAX_URLS_TO_CRAWL}`)
  .action((url) => {
    if (!url) {
      askForInput();
      return;
    }
    let cleanRoot;
    try {
      cleanRoot = new URL(url).toString();
    } catch (e) {
      askForInput();
      return;
    }

    crawl(cleanRoot, Number(program.limit), program.progress).then(assets => {
      console.log(assets);
    });
  })
  .parse(process.argv);
