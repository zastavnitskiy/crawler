/**
 * This module is responsible for parsing html document and returning DOM tree.
 * we use external module for that and relying on the tree structure that is provided
 * by that module.
 */

var htmlparser = require('htmlparser');

function parse(html) {
  return new Promise((resolve, reject) => {
    if (typeof html === 'undefined') {
      reject(`parseHTML expected string as input, got ${html}`);
    }

    var parser = new htmlparser.Parser(
      new htmlparser.DefaultHandler(function(error, dom) {
        if (error) {
          reject(error);
        }

        resolve(dom);
      })
    );

    parser.parseComplete(html);
  });
}

module.exports = parse;
