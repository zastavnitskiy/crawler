/**
 * crawl is the module that does actual crawling.
 * it implmements a combination of a Set and a Queue – fifo data structure, that
 * only allows each item once. We stop crawling once queue is empty or we reach the limit
 * of pages to crawl.
 */
const download = require("./download");
const traverse = require("./traverse");
const parseHTML = require("./parseHTML");
const isSameDomainLink = require("./utils/isSameDomainLink");
const DEFAULT_MAX_URLS_TO_CRAWL = 1000;
const URL = require("url").URL;
const SetQueue = require("./utils/SetQueue");

/* Once  */
function catchError(message, returnValue, e) {
  console.warn(
    message,
    e
  );
  return Promise.resolve(returnValue);
}

function prepareAssets(crawled) {
  return Object.keys(crawled).map(url => ({
    url: url,
    assets: crawled[url].assets
  }));
}

function crawl(root, limit = DEFAULT_MAX_URLS_TO_CRAWL) {
  const crawled = {};
  const crawlQueue = new SetQueue();
  return new Promise((resolve, reject) => {
    function next() {
      // let's not hit stack limitations
      process.nextTick(() => {
        const crawledSoFar = Object.keys(crawled).length;

        process.env.NODE_ENV === "development" &&
          console.log(
            `Crawled ${crawledSoFar}, queue: ${crawlQueue.length}, processed ${crawlQueue.processed} `
          );

        if (crawlQueue.length === 0) {
          resolve(prepareAssets(crawled));
          return;
        }

        if (crawledSoFar >= limit) {
          resolve(prepareAssets(crawled));
          return;
        }

        const nextRawUrl = crawlQueue.next();
        let nextUrl;
        try {
          nextUrl = new URL(nextRawUrl).toString();
        } catch (e) {
          console.warn(
            `unable to proceed next value in the queue: ${nextRawUrl} , skipping`
          );
          next();
        }

        if (nextUrl) {
          download(nextUrl)
            .catch(catchError.bind(null, `We failed to download ${nextUrl}, will proceed with the queue`, ''))
            .then(parseHTML)
            .catch(catchError.bind(null, `We failed to parse ${nextUrl}, will proceed with the queue`, []))
            .then(traverse)
            .catch(catchError.bind(null, `We failed to traverse ${nextUrl}, will proceed with the queue`, { links: [], assets: []}))
            .then(rawResults => {
              if (rawResults) {
                const rawLinks = rawResults.links || [];
                const links = rawLinks
                  .filter(link => {
                    return isSameDomainLink(nextUrl, link);
                  })
                  .map(link => {
                    return new URL(link, nextUrl).toString();
                  });

                const rawAssets = rawResults.assets || [];
                const assets = rawAssets.map(asset => {
                  return new URL(asset, nextUrl).toString();
                });

                const results = {
                  links: links,
                  assets: assets,
                  url: nextUrl.toString()
                };

                links.forEach(link => {
                  crawlQueue.add(link);
                });

                crawled[nextUrl] = results;
              }

              next();
            })
            .catch(catchError.bind(null, 'We failed to process results of ${nextUrl}, will continue with the queue', {}));
        }
      });
    }

    if (typeof root === "undefined") {
      reject(`Crawl expects url as first parameter, received ${root}`);
    }

    crawlQueue.add(root);

    next();
  });
}

crawl.DEFAULT_MAX_URLS_TO_CRAWL = DEFAULT_MAX_URLS_TO_CRAWL;

module.exports = crawl;
