var download = require('./download');

it('returns a promise', () => {
  var returnValue = download('http://success.com/');
  expect(returnValue instanceof Promise).toBeTruthy();
});

it('rejects if page unavailable', () => {
  expect(download('http://failure.com/')).rejects.toBeDefined();
});

it('resolves with page content', () => {
  expect(download('http://success.com/')).resolves.toMatchSnapshot();
});
