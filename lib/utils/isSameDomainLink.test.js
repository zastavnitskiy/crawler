var isSameDomainLink = require('./isSameDomainLink');

[
  [
    'http://www.youtube.com/watch?v=ClkQA2Lb_iE',
    'https://www.youtube.com/watch?v=ClkQA2Lb_iE',
  ],
  [
    'https://www.youtube.com/watch?v=ClkQA2Lb_iE',
    'www.youtube.com/watch?v=ClkQA2Lb_iE',
  ],
  ['https://www.youtube.com/watch?v=ClkQA2Lb_iE', '/watch?v=ClkQA2Lb_iE'],
  //todo: we will need to clarify, we we want to treat www as same domain url
  // [
  //   "https://www.youtube.com/watch?v=ClkQA2Lb_iE",
  //   "youtube.com/watch?v=ClkQA2Lb_iE"
  // ]
].forEach(pair => {
  it(`same domain ${pair[0]} — ${pair[1]}`, () =>
    expect(isSameDomainLink.apply(null, pair)).toBeTruthy());
});

[
  [
    'www.youtube.com/watch?v=ClkQA2Lb_iE',
    'ftps://ftp.websitename.com/dir/file.txt',
  ],
  ['websitename.com:1234/dir/file.txt', 'example.com?param=value'],
  ['test.com', '#test.com'],
  ['#', '#'],
  ['', ''],
].forEach(pair => {
  it(`different domains: ${pair[0]} — ${pair[1]}`, () =>
    expect(isSameDomainLink.apply(null, pair)).toBeFalsy());
});
