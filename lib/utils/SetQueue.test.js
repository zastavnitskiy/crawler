const SetQueue = require("./SetQueue");

it("should allow adding items", () => {
  const links = new SetQueue();

  links.add("test.com");
  expect(links.length).toEqual(1);
  expect(links.next()).toEqual("test.com");
  expect(links.length).toEqual(0);
});

it("should allow taking items", () => {
  const links = new SetQueue();
  links.add("test.com");
  expect(links.length).toEqual(1);
  expect(links.next()).toEqual("test.com");
  expect(links.length).toEqual(0);
});
it("should maintain FIFO order", () => {
  const links = new SetQueue();

  links.add("first");
  links.add("second");
  expect(links.length).toEqual(2);
  expect(links.next()).toEqual("first");
  expect(links.next()).toEqual("second");
  expect(links.length).toEqual(0);
});

it("should ignore items that are in the SetQueue", () => {
  const links = new SetQueue();
  let undef;
  links.add("first");
  links.add("first");
  expect(links.length).toEqual(1);
  expect(links.next()).toEqual("first");
  expect(links.next()).toEqual(undef);
  expect(links.length).toEqual(0);
});

it("should ignore items that have already been in the SetQueue", () => {
  const links = new SetQueue();
  let undef;
  links.add("first");

  expect(links.length).toEqual(1);
  expect(links.next()).toEqual("first");
  links.add("first");
  expect(links.length).toEqual(0);
  expect(links.next()).toEqual(undef);
  expect(links.length).toEqual(0);
});

it("should count processed items", () => {
  const links = new SetQueue();
  links.add("first");
  expect(links.length).toEqual(1);
  expect(links.next()).toEqual("first");
  links.add("second");
  links.add("first");
  expect(links.next()).toEqual('second');
  expect(links.length).toEqual(0);
  expect(links.processed).toEqual(2);
});
