var extractHostname = require('./extractHostname');

function isSameDomain(currentUrl, linkUrl) {
  if (!linkUrl) {
    return false;
  }

  if (linkUrl.startsWith('/')) {
    return true;
  }

  if (linkUrl.startsWith('#')) {
    return false;
  }

  if (linkUrl.startsWith('javascript:void(0)')) {
    return false;
  }

  var currentHostname = extractHostname(currentUrl);
  var linkHostname = extractHostname(linkUrl);

  return currentHostname === linkHostname;
}

module.exports = isSameDomain;
