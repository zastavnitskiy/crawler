var extractHostname = require('./extractHostname');

[
  'http://www.youtube.com/watch?v=ClkQA2Lb_iE',
  'https://www.youtube.com/watch?v=ClkQA2Lb_iE',
  'www.youtube.com/watch?v=ClkQA2Lb_iE',
  'ftps://ftp.websitename.com/dir/file.txt',
  'websitename.com:1234/dir/file.txt',
  'ftps://websitename.com:1234/dir/file.txt',
  'example.com?param=value',
].forEach(url => {
  it(`should extract host properly from ${url}`, () =>
    expect(extractHostname(url)).toMatchSnapshot());
});

it('should handle hash urls properly', () => {
  expect(extractHostname(' #http://test.com')).toEqual('');
});

it('should handle javascript urls properly', () => {
  expect(extractHostname(' javascript:(alert("http://test.com")')).toEqual('');
});
