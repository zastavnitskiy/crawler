class QueueSet {
  constructor() {
    this.items = [];
    this.index = {};
  }

  get length() {
    return this.items.length;
  }

  get processed() {
    return Object.keys(this.index).length;
  }

  add(item) {
    if (!this.index[item]) {
        this.items.push(item);
        this.index[item] = true;
    };

  }

  next() {
    return this.items.shift();
  }
}

module.exports = QueueSet;
