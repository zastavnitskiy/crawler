const crawl = require("./crawl");

it("should return the promise", () => {
  var returnValue = crawl("http://success.com/");
  expect(returnValue instanceof Promise).toBeTruthy();
});

it("should reject is executed with wrong parameters", () => {
  var returnValue = crawl();

  return expect(returnValue).rejects.toBeTruthy();
});

it("should crawl test url", () => {
  expect.assertions(1);
  return expect(crawl("http://success.com/")).resolves.toBeTruthy();
});

it("should not reject of something happened along the way", () => {
  //todo: test for warning here
  // console.warn = jest.fn();
  expect.assertions(1);
  return expect(crawl("http://failure.com/")).resolves.toBeTruthy();
});

it("should parse all the assets", () => {
  expect.assertions(1);
  return expect(crawl('http://test.com')).resolves.toMatchSnapshot();
});

it("should not crawl external urls", () => {
  expect.assertions(1);
  return expect(crawl('http://test.com?include-google')).resolves.toMatchSnapshot();
});

it("should crawl relative URLS", () => {
  expect.assertions(1)
  return crawl('http://test-relative-urls.com/').then((results) => {
    console.warn('relative', results)
    expect(results.length).toEqual(2);
    return results;
  }).catch(e => {
    console.log('error', e);
  });
})
