var traverse = require('./traverse');

it('should throw if executed with not an array', () => {
  var undef;
  expect(() => traverse(undef)).toThrow();
  expect(() => traverse({})).toThrow();
  expect(() => traverse('<div></div>')).toThrow();
});

it('should traverse first level', () => {
  expect(
    traverse([
      {
        type: 'tag',
        name: 'a',
        attribs: { href: 'test.com' },
      },
      {
        type: 'tag',
        name: 'link',
        attribs: { rel: 'stylesheet', href: 'test.css' },
      },
    ])
  ).toMatchSnapshot();
});
it('should recursively traverse and concatenate the results', () => {
  expect(
    traverse([
      {
        type: 'tag',
        name: 'a',
        attribs: { href: 'test.com' },
      },
      {
        type: 'tag',
        name: 'link',
        attribs: { rel: 'stylesheet', href: 'test.css' },
        children: [
          {
            type: 'tag',
            name: 'a',
            attribs: { href: 'test-deep.com' },
          },
          {
            type: 'tag',
            name: 'link',
            attribs: { rel: 'stylesheet', href: 'test-deep.css' },
          },
        ],
      },
    ])
  ).toMatchSnapshot();
});
