var request = require('request');

var requests = {
  'http://success.com/': {
    html: '<div>Success</div>',
  },
  'http://failure.com/': {
    error: `Error when fetching "http://failure.com"`,
  },
  'http://test.com/': {
    html: `<div>
            <link rel="stylesheet" href="./test.css">
            <a href="http://test.com/page.html">Page</a>
          </div>`,
  },
  'http://test.com/page.html': {
    html: `<div>
            <link rel="stylesheet" href="./test.css">
            <link rel="stylesheet" href="./page.css">
            <a href="http://test.com/about.html">About</a>
            </div>`,
  },
  'http://test.com/about.html': {
    html: `<div>
            <link rel="stylesheet" href="./test.css">
            <link rel="stylesheet" href="./about.css">
            <a href="http://test.com/page.html">About</a>
            <script src="./script-about.js"></script>
            </div>`,
  },
  'http://test.com/?include-google': {
    html: `<div>
            <link rel="stylesheet" href="./test.css">
            <a href="http://test.com/page.html">Page</a>
            <a href="http://google.com/">Google</a>
          </div>`,
  },
  'http://test-relative-urls.com/': {
    html: "<a href='/relative.html'></a>"
  },
  'http://test-relative-urls.com/relative.html': {
    html: "Success"
  }
};

module.exports = function mockedRequest(url, callback) {
  var mocked = requests[url];
  if (mocked) {
    if (mocked.error) {
      callback(mocked.error, mocked.html, mocked.html);
    } else {
      callback(null, mocked.html, mocked.html);
    }
  } else {
    console.warn(`Request mock: ${url} doesn't seem to be mocked.`, "", "");
    callback(`Request mock: ${url} doesn't seem to be mocked.`, "", "");
  }
};
