/*
Download is a simple wrapper over request module, that provides us with a promise.
Once we will use node 8, util promisify can replace this module.
The promise is resolved with page body if request succeeds.
 */
var request = require('request');

function download(url) {
  return new Promise((resolve, reject) => {
    request(url, (error, html, body) => {
      if (error) {
        reject(`Error when downloading ${url}.`);
      }
      resolve(body);
    });
  });
}

module.exports = download;
