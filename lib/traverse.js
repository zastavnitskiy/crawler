/*
The goal of traverse module is to process the DOM tree, and pick up links to other pages and assets,
images and scripts from it.
As a result we expect an onject with assets and links arrays in it.
 */

function traverse(nodes) {
  if (!Array.isArray(nodes)) {
    throw new Error(`traverse expected 1 array parameters, got ${nodes}`);
  }

  var result = nodes.reduce(
    (result, node) => {
      if (node.type === 'tag') {
        if (node.name === 'a') {
          if (node.attribs && node.attribs.href) {
            result.links.push(node.attribs.href.trim());
          }
        }

        if (node.name === 'img') {
          if (node.attribs && node.attribs.src) {
            result.assets.push(node.attribs.src.trim());
          }
        }

        if (node.name === 'link') {
          if (node.attribs && node.attribs.href && node.attribs.rel === 'stylesheet') {
            result.assets.push(node.attribs.href.trim());
          }
        }
      }

      if (node.type === 'script') {
        if (node.attribs && node.attribs.src) {
          result.assets.push(node.attribs.src.trim());
        }
      }

      if (node.children) {
        var subtree = traverse(node.children);

        result.links = result.links.concat(subtree.links);
        result.assets = result.assets.concat(subtree.assets);
      }

      return result;
    },
    {
      links: [],
      assets: [],
    }
  );

  return result;
}

module.exports = traverse;
