var parseHTML = require('./parseHTML');

it('returns a promise', () => {
  var returnValue = parseHTML('<div></div>');
  expect(returnValue instanceof Promise).toBeTruthy();
});

it('resolves a promise with dom', () => {
  parseHTML('<div></div>').then(dom => {
    const shot = JSON.stringify(dom, null, 2);
    expect(shot).toMatchSnapshot();
  });
});

it('rejects if executed with undedefined', () => {
  var undef;
  expect(parseHTML(undef)).rejects.toMatchSnapshot();
});

it('parses script tags', () => {
  parseHTML('<script src="./script.js"></script>').then(nodes => {
    expect(nodes).toBeDefined();
    expect(nodes.length).toEqual(1);
    expect(nodes[0].type).toEqual('script');
    expect(nodes[0].attribs.src).toEqual('./script.js');
  });
});

it('parses links tags', () => {
  parseHTML('<link href="./style.css" />').then(nodes => {
    expect(nodes).toBeDefined();
    expect(nodes.length).toEqual(1);
    expect(nodes[0].type).toEqual('tag');
    expect(nodes[0].name).toEqual('link');
    expect(nodes[0].attribs.href).toEqual('./style.css');
  });
});

it('parses a tags', () => {
  parseHTML('<a href="http://site.com">Test</a>').then(nodes => {
    expect(nodes).toBeDefined();
    expect(nodes.length).toEqual(1);
    expect(nodes[0].type).toEqual('tag');
    expect(nodes[0].name).toEqual('a');
    expect(nodes[0].attribs.href).toEqual('http://site.com');
  });
});

it('parses img tags', () => {
  parseHTML('<img src="http://site.com/image.gif" />').then(nodes => {
    expect(nodes).toBeDefined();
    expect(nodes.length).toEqual(1);
    expect(nodes[0].type).toEqual('tag');
    expect(nodes[0].name).toEqual('img');
    expect(nodes[0].attribs.src).toEqual('http://site.com/image.gif');
  });
});
